#!/bin/sh

xmenu <<EOF | sh
[]= Tiled Layout	echo 0
><> Floating Layout	echo 1
[M] Monocle Layout	echo 2
EOF
